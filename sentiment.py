import pickle
from nltk.stem.wordnet import WordNetLemmatizer
from nltk import word_tokenize

def load_dict(resourcepath):
    """ Loads the Sentiment dictionary from the pickle in resourcepath.p.
    The dictionary persists across invocations of get_score.
    The dictionary has keys as words and values as the sentiment score associated with the word.
    """

    if load_dict.swn_dict is None:   # Not already loaded. Load now.
        with open("{0}.p".format(resourcepath), "rb") as swn:
            load_dict.swn_dict = pickle.load(swn)

def clear_dict():
    load_dict.swn_dict = None

load_dict.swn_dict = None

def load_dataset(datapath):
    """ Accepts a folder path and loads info about all documents in the folder in a dict.
    Assumed folder structure:
    - datapath
    --- class1
    ----- documents
    --- class2
    ----- documents, etc.
    """

    import os

    doc_dict_list = []

    for classname in os.listdir(datapath):
        for filename in os.listdir("{0}/{1}".format(datapath, classname)):
            doc_dict = {}
            doc_dict["filepath"] = "{0}{1}/{2}".format(datapath, classname, filename)
            doc_dict["class"] = classname
            doc_dict_list.append(doc_dict)

    return doc_dict_list

def analyse_document(filepath, resourcepath):
    """ Accepts a filepath and a resourcepath (without extension) and analyses the file using the resource.
    Here, filepath = Path to the text file to be analysed
    resourcepath = Path to sentiment resource to be used. Don't use extension, the '.p' extension is added automatically.
    """
    f = open(filepath)
    filetext = f.read()

    return analyse_text(filetext, resourcepath)

def analyse_text(filetext, resourcepath):
    """ Called by analyse_document with 2 parameters: the actual text of the document and the resourcepath.
    Assumes the score to be a single real number centered at 0.
    Positive numbers indicate positive sentiment while negative numbers indicate negative sentiment.
    """

    total_score = 0
    lemmatizer = WordNetLemmatizer()
    for word in word_tokenize(filetext):
        word = word.lower()   # TODO: Handle words with punctuations. Preferably get hold of a good word tokenizer
        lemmatized_word = lemmatizer.lemmatize(word)
        total_score += get_score(lemmatized_word, resourcepath)   # Simple summation of scores. TODO: Explore SO-CAL-like features
    return total_score
    # if(total_score >= 0):
        # return 'pos'
    # else:
        # return 'neg'

def analyse_dataset(dataset, resourcepath, cutoff=0):
    """ Accepts a dataset as a dict and returns a modified dict containing additional fields after analysis of document.
    """

    analysed_dataset = []

    for doc_dict in dataset:
        analysed_dict = doc_dict
        score = analyse_document(doc_dict["filepath"], resourcepath)
        analysed_dict["score"] = score
        analysed_dict["predictedclass"] = "pos" if score >= cutoff else "neg"
        analysed_dataset.append(analysed_dict)

    return analysed_dataset
        

def get_score(word, resourcepath):
    """ Retrieves the sentiment score of a particular word.
    """
    load_dict(resourcepath)   # Load dict if not already loaded
    try:
        score = load_dict.swn_dict[word]
    except KeyError:   # Word does not exist in lexicon
        score = 0
    return score

def get_accuracy(analysed_dataset):
    """ Accepts an analysed dataset and returns the accuracy
    """

    correct = 0

    for doc_dict in analysed_dataset:
        if doc_dict["class"] == doc_dict["predictedclass"]:
            correct += 1

    accuracy = correct / len(analysed_dataset)
    return accuracy
